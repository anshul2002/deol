import { Component, OnInit } from '@angular/core';
import { EmailValidator, FormControl, FormGroup, Validators } from '@angular/forms';
import { Route, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-commerical',
  templateUrl: './commerical.component.html',
  styleUrls: ['./commerical.component.css']
})
export class CommericalComponent implements OnInit{

  commericalForm!:FormGroup
  issubmitForm:boolean=false


  constructor(public Route:Router,public toaster:ToastrService){}
  ngOnInit() {
    this.initcommericalForm();
  }

initcommericalForm(){
  this.commericalForm= new FormGroup({
    firstName :new FormControl('',Validators.required),
    lastname :new FormControl('',Validators.required),
    email:new FormControl('',[Validators.required,Validators.email]),
    number:new FormControl('',Validators.required),
    companyname:new FormControl('',Validators.required),
    amount:new FormControl('',Validators.required,),
    days:new FormControl('',Validators.required)
    })

  


}
  
onSearch(){
this.issubmitForm=true
if(this.commericalForm.valid){
  console.log("commerical form value",this.commericalForm.value)

}
else{
  console.log("commerical Form is invalid");
  this.toaster.error('please fill the all data');
}
  }
}
