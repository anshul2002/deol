import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DandenongComponent } from './dandenong.component';

describe('DandenongComponent', () => {
  let component: DandenongComponent;
  let fixture: ComponentFixture<DandenongComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DandenongComponent]
    });
    fixture = TestBed.createComponent(DandenongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
