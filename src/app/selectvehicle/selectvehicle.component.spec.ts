import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectvehicleComponent } from './selectvehicle.component';

describe('SelectvehicleComponent', () => {
  let component: SelectvehicleComponent;
  let fixture: ComponentFixture<SelectvehicleComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [SelectvehicleComponent]
    });
    fixture = TestBed.createComponent(SelectvehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
