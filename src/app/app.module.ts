import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { LocationComponent } from './location/location.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { ReservationComponent } from './reservation/reservation.component';
import { CommericalComponent } from './commerical/commerical.component';
import { BlogsComponent } from './blogs/blogs.component';
import { DandenongComponent } from './dandenong/dandenong.component';
import { SydneyComponent } from './sydney/sydney.component';
import { AdelaideComponent } from './adelaide/adelaide.component';
import { TullamarineComponent } from './tullamarine/tullamarine.component';
import { AddonCardComponent } from './addon-card/addon-card.component';
import { SelectvehicleComponent } from './selectvehicle/selectvehicle.component';
import { YourinformationComponent } from './yourinformation/yourinformation.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ToastrModule } from 'ngx-toastr';
import { LocationdetailsComponent } from './locationdetails/locationdetails.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    LocationComponent,
    VehicleComponent,
    ReservationComponent,
    CommericalComponent,
    BlogsComponent,
    DandenongComponent,
    SydneyComponent,
    AdelaideComponent,
    TullamarineComponent,
    AddonCardComponent,
    SelectvehicleComponent,
    YourinformationComponent,
    AboutusComponent,
    ContactusComponent,
    LocationdetailsComponent,
  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    ToastrModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
