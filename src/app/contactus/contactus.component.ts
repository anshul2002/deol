import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactusComponent implements OnInit {
  contactForm!: FormGroup;
  issubmitForm: boolean = false;

  constructor(public route: Router) {}

  ngOnInit() {
    this.initContactForm();
  }

  initContactForm() {
    this.contactForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      email: new FormControl('',[Validators.required,Validators.email]),
      phone: new FormControl('',Validators.required),
      message: new FormControl('',Validators.required)
    });
  }

  onSubmit() {
    this.issubmitForm = true;
    if (this.contactForm.valid) {
      console.log("Contact Form Values:", this.contactForm.value);
    } else {
      console.log("Contact Form is invalid");
    }
  }
}
