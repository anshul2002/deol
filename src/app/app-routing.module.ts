import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LocationComponent } from './location/location.component';
import { VehicleComponent } from './vehicle/vehicle.component';
import { ReservationComponent } from './reservation/reservation.component';
import { CommericalComponent } from './commerical/commerical.component';
import { BlogsComponent } from './blogs/blogs.component';
import { DandenongComponent } from './dandenong/dandenong.component';
import { SydneyComponent } from './sydney/sydney.component';
import { AdelaideComponent } from './adelaide/adelaide.component';
import { TullamarineComponent } from './tullamarine/tullamarine.component';
import { SelectvehicleComponent } from './selectvehicle/selectvehicle.component';
import { YourinformationComponent } from './yourinformation/yourinformation.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactusComponent } from './contactus/contactus.component';

const routes: Routes = [
{
  path:'',
  redirectTo:'/home',
  pathMatch:'full',
},
{
  path:"home",
  component:HomeComponent,
},
{
  path:"location",
  component:LocationComponent,
},
{
  path:"vehicle",
  component:VehicleComponent,
},
{
  path:"reservation",
  component:ReservationComponent,
},
{
  path:"commerical",
  component:CommericalComponent,
},
{
  path:"blogs",
  component:BlogsComponent,
},
{
  path:"dandenong",
  component:DandenongComponent
},
{
  path:"sydney",
  component:SydneyComponent,
},
{
  path:"adelaide",
  component:AdelaideComponent,

},
{
  path:"tullamarine",
  component:TullamarineComponent,
},

{
  path:"selectvehicle",
  component:SelectvehicleComponent,
},

{
  path:"yourinformation",
  component:YourinformationComponent,
},

{
  path:"aboutus",
  component:AboutusComponent,
},

{
  path:"contactus",
  component:ContactusComponent,
},

];


@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash:true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
