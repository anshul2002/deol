import { ComponentFixture, TestBed } from '@angular/core/testing';

import { YourinformationComponent } from './yourinformation.component';

describe('YourinformationComponent', () => {
  let component: YourinformationComponent;
  let fixture: ComponentFixture<YourinformationComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [YourinformationComponent]
    });
    fixture = TestBed.createComponent(YourinformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
