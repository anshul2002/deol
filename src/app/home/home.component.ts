import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrModule, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

})
export class HomeComponent implements OnInit {

  
image:{url:string,alt:string}[ ]=[{
  url:"/assets/firstcar.jpeg",alt:"slider1"
},
{url:"/assets/secondimg.jpeg",alt:"slider2" },
{url:"/assets/thirdimg.jpeg",alt:"slider3"},
{url:"/assets/fourthimg.jpeg",alt:"slider4"},
{url:"/assets/fifthimg.jpg",alt:"slider5"},

]
  currentsliderIndex: number=0;
d: any;
formData: any;
searchForm:any;
issubmitForm:boolean=false;
constructor(public route: Router,public toaster:ToastrService){

}
  ngOnInit() {
    this.initsearchForm();
  setInterval(()=>{
    this.nextslider();
  
  },5000)
  }
  initsearchForm(){
    this.searchForm=new FormGroup({
      pickuplocation:new FormControl('',Validators.required),
      droplocation:new FormControl('',Validators.required),
      pickupdate:new FormControl('',Validators.required),
      dropdate:new FormControl('',Validators.required),
      all:new FormControl('all',Validators.required),
    })

  }
  nextslider(){
    this.currentsliderIndex=(this.currentsliderIndex +1)% this.image.length
  }
  onSearch(){
    console.log("searchForm",this.searchForm.value)
   this.issubmitForm= true;
   if(this.searchForm.valid){
    localStorage.setItem('searchForm',JSON.stringify(this.searchForm.value));
    this.route.navigate(['/selectvehicle'])
   }
   else{
    this.toaster.error('please fill the all data');
   }
  }


}
