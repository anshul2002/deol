import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TullamarineComponent } from './tullamarine.component';

describe('TullamarineComponent', () => {
  let component: TullamarineComponent;
  let fixture: ComponentFixture<TullamarineComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TullamarineComponent]
    });
    fixture = TestBed.createComponent(TullamarineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
